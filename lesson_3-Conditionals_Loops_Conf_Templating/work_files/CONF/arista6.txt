!
transceiver qsfp default-mode 4x10G
!
hostname arista6
!
ntp server 0.europe.pool.ntp.org
!
spanning-tree mode rapid-pvst
!
aaa authorization exec default local
!
no aaa root
!
!
clock timezone Europe/Dublin
!
vlan 2-7
!
interface "Ethernet1"
   spanning-tree portfast
   spanning-tree cost 1
!
interface 2
   switchport access vlan 2
!
interface 3
   switchport access vlan 3
!
interface 4
   switchport access vlan 4
!
interface 5
   switchport access vlan 5
!
interface 6
   switchport access vlan 6
!
interface 7
   switchport access vlan 7
!
!
interface Management1
   shutdown
!
interface Vlan1
   ip address 10.220.88.33/24
!
ip route 0.0.0.0/0 10.220.88.1 
!
ip routing
!
management api http-commands
   no shutdown
!
end
