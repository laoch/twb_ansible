#!/usr/bin/env python3
import sys
import yaml
from pprint import pprint


def read_yaml(filename):
    with open(filename) as fh:
        return yaml.safe_load(fh)

if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except IndexError:
        filename = input("Enter YAML file name: ")
    print(type(read_yaml(filename)), read_yaml(filename))
