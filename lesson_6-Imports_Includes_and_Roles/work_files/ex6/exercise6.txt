#!/usr/bin/env bash

/*

.
├── exercise6.yaml
└── roles
    └── vrf_lite
        ├── tasks
        │   └── main.yaml
        ├── templates
        │   └── vrf_lite.j2
        └── vars
            └── main.yaml


*/

mkdir roles
mkdir roles/vrf_lite
mkdir roles/vrf_lite/tasks
mkdir roles/vrf_lite/templates
mkdir roles/vrf_lite/vars

cat <<EOM > roles/vrf_lite/vars/main.yaml
---
vrfs:
  - vrf_name: blue
    route_distinguisher: "65000:1"
    interfaces:
      - Loopback98
  - vrf_name: red 
    route_distinguisher: "65000:2"
    interfaces:
      - Loopback99
...
EOM

cat <<EOM > roles/vrf_lite/templates/vrf_lite.j2
{% for vrf_dict in vrfs %}
ip vrf {{ vrf_dict.vrf_name }}
 rd {{ vrf_dict.route_distinguisher }}
!
{% for intf in vrf_dict.interfaces %}
interface {{ intf }}
 ip vrf forwarding {{ vrf_dict.vrf_name }}
!
{% endfor %}
{% endfor %}
EOM


cat <<EOM > roles/vrf_lite/tasks/main.yaml
- name: Debugging of Template
  ansible.builtin.set_fact:
    vrf_config: "{{ lookup('template', 'vrf_lite.j2') }}"
  tags: never

- ansible.builtin.debug:
    msg: "{{ vrf_config.splitlines() }}"
  tags: never

- name: Push configuration
  cisco.ios.ios_config:
    src: vrf_lite.j2
...
EOM


cat <<EOM > exercise6.yaml
---
- name: Configure VRF Lite
  hosts: cisco5:cisco6
  gather_facts: False
  tasks:
    - name: VRF Lite Role
      ansible.builtin.import_role: 
        name: vrf_lite
...
EOM


ansible-playbook exercise6.yaml

PLAY [Configure VRF Lite] *************************************************************************

TASK [vrf_lite : Push configuration] **************************************************************
changed: [cisco6]
changed: [cisco5]

PLAY RECAP ****************************************************************************************
cisco5                     : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
cisco6                     : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   


