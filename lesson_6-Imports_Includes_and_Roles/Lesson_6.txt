The video password is: Newton27

I.  Dynamic (include) vs Static (import)       | https://vimeo.com/392590801  | 11 minutes
II. Import-Include and Tags                    | https://vimeo.com/393013079  | 10 minutes
III. Import-Include and Conditionals           | https://vimeo.com/392609877  | 6 minutes
IV. Dynamic or Static - Which should you use?  | https://vimeo.com/392611658  | 3 minutes
V.  include_tasks and import_tasks             | https://vimeo.com/393027658  | 12 minutes
VI. include_vars                               | https://vimeo.com/393049779  | 4 minutes
VII. Roles                                     | https://vimeo.com/393584559  | 13 minutes
VIII. Roles (Part2)                            | https://vimeo.com/393817257  | 5 minutes
IX. Playbook Composition                       | https://vimeo.com/393821476  | 10 minutes
