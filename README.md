# Ansible Course  
  
## lesson_1-Ansible_Fundamentals  
Lesson_1.odt    
Lesson_1.txt    
print_yaml.py     
work_files    
├── 1a.MyFile.yml  
├── 1b.MyFile.yml  
├── 1c.MyFile.yml  
├── 1d.MyFile.yml  
├── 1e.MyFile.yml  
├── exercise3a.yaml  
├── exercise3b.yaml  
├── exercise3c.yaml  
├── inventory2a.ini  
├── inventory2b.ini  
├── inventory2c.ini  
└── print_yaml.py   

## lesson_2-Modules_Network_Fact_Gathering  
Lesson_2.odt  
Lesson_2.txt  
work_files  
 ├── ansible-hosts.ini.bak  
 ├── exercise1a.yaml  
 ├── exercise1b.yaml  
 ├── exercise1c.yaml  
 ├── exercise1d.yaml  
 ├── exercise1e.yaml  
 ├── exercise1.yaml  
 ├── exercise2a.yaml  
 ├── exercise2b.yaml  
 ├── exercise2c.yaml  
 ├── exercise3a.yaml  
 ├── exercise3b.yaml  
 ├── exercise3c.yaml  
 ├── exercise4a.yaml  
 ├── exercise5a.yaml  
 ├── exercise5b.yaml  
 ├── exercise5c.yaml  
 ├── exercise6a.yaml  
 ├── exercise6b.yaml  
 ├── group_vars  
 │     ├── all.yml  
 │     └── cisco  
 │         └── bgp.yml  
 ├── host_vars  
 │     ├── cisco1  
 │     │     ├── bgp.yml  
 │     │     └── ip_addresses.yml  
 │     ├── cisco2  
 │     │     ├── bgp.yml  
 │     │     └── ip_addresses.yml  
 │     ├── cisco5  
 │     │     ├── bgp.yml  
 │     │     └── ip_addresses.yml  
 │     ├── cisco6  
 │     │     ├── bgp.yml  
 │     │     └── ip_addresses.yml  
 │     └── cisco7.yml  
 └── my_vars.yml  
  
## lesson_3-Conditionals_Loops_Conf_Templating  
Lesson_3.odt  
Lesson_3.txt  
work_files  
 ├── arista_template.j2  
 ├── ASSEMBLE  
 │     └── nxos  
 │         ├── 10-ip.cfg  
 │         └── 20-bgp.cfg  
 ├── CONF  
 │     ├── arista5.txt  
 │     ├── arista6.txt  
 │     ├── arista7.txt  
 │     ├── arista8.txt  
 │     └── nxos.cfg  
 ├── ex5_ktb.yaml  
 ├── exercise1a.yaml  
 ├── exercise1b.yaml  
 ├── exercise2.yaml  
 ├── exercise3.yaml  
 ├── exercise4.yaml  
 ├── exercise5.yaml  
 ├── group_vars  
 │     ├── all.yml  
 │     ├── arista.yml  
 │     └── nxos.yml  
 ├── host_vars  
 │     ├── arista5.yml  
 │     ├── arista6.yml  
 │     ├── arista7.yml  
 │     ├── arista8.yml  
 │     ├── arista.yml  
 │     ├── nxos1.yml  
 │     └── nxos2.yml  
 └── templates  
     ├── nxos_bgp.j2  
         └── nxos_ip.j2  
  
## lesson_4-Making_Network_Configuration_Changes  
Lesson_4.odt  
Lesson_4.txt  
work_files  
 ├── arista_template.j2  
 ├── exercise1.yml  
 ├── exercise2.yaml  
 ├── exercise3a.yaml  
 ├── exercise3b.yaml  
 ├── exercise4a.yaml  
 ├── exercise4b.yaml  
 ├── exercise4c.yaml  
 ├── exercise4d.yaml  
 ├── exercise5a.yaml  
 ├── exercise5b.yaml  
 ├── exercise5c.yaml  
 ├── exercise5d.yaml  
 ├── group_vars  
 │     ├── all.yml  
 │     ├── arista.yml  
 │     └── nxos.yml  
 └── host_vars  
     ├── cisco1.yml  
     ├── nxos1.yml  
     └── nxos2.yml  
  
  
## lesson_5-Making_Network_Configuration_Changes_2  
├── Lesson_5.odt  
├── Lesson_5.txt  
└── work_files  
   ├── CONF  
   │     ├── nxos1.cfg  
   │     └── nxos2.cfg  
   ├── exercise1.yaml  
   ├── exercise2.yaml  
   ├── exercise3a.yaml  
   ├── exercise3b.yaml  
   ├── exercise4.yaml  
   ├── exercise5.yaml  
   ├── group_vars  
   │     └── all.yaml  
   └── j2_templates  
       ├── eos  
       │     └── dt_config.j2  
       ├── ios  
       │     └── dt_config.j2  
       ├── junos  
       │     └── dt_config.j2  
       └── nxos  
           └── dt_config.j2  
  
## lesson_6-Imports_Includes_and_Roles  
├── ex1  
│     ├── exercise1a.yaml  
│     ├── exercise1b.yaml  
│     ├── exercise1c.yaml  
│     ├── host_vars  
│     │     └── localhost.yaml  
│     ├── subtask1.yaml  
│     └── subtask2.yaml  
├── ex2  
│     ├── exercise2a.yaml  
│     ├── exercise2b.yaml  
│     ├── exercise2c.yaml  
│     ├── host_vars  
│     │     └── localhost.yaml  
│     ├── subtask1.yaml  
│     └── subtask2.yaml  
├── ex3  
│     ├── exercise3.yaml  
│     ├── group_vars  
│     │     └── all.yaml  
│     ├── ios-task.yaml  
│     └── iosxe-task.yaml  
├── ex4  
│     ├── eos-task.yaml  
│     ├── exercise4.yaml  
│     ├── group_vars  
│     │     └── all.yaml  
│     ├── ios-task.yaml  
│     ├── iosxe-task.yaml  
│     └── nxos-task.yaml  
├── ex5  
│     ├── exercise5.txt  
│     ├── exercise5.yaml  
│     └── roles  
│         └── ntp  
│             ├── tasks  
│             │     └── main.yaml  
│             ├── templates  
│             │     ├── ntp.j2  
│             │     └── ntp_nxos.j2  
│             └── vars  
│                 └── main.yaml  
└── ex6  
   ├── exercise6.txt  
   ├── exercise6.yaml  
   └── roles  
       └── vrf_lite  
           ├── tasks  
           │     └── main.yaml  
           ├── templates  
           │     └── vrf_lite.j2  
           └── vars  
               └── main.yaml  
  
## lesson_7-Parsers_and_Dynamic_Inventory  
├── Lesson_7.odt  
├── Lesson_7.txt  
├── playground  
└── work_files  
   ├── ex1  
   │     ├── exercise1.yaml  
   │     ├── group_vars  
   │     │     ├── all.yaml  
   │     │     └── nxos.yaml  
   │     ├── host_vars  
   │     │     ├── nxos1.yaml  
   │     │     └── nxos2.yaml  
   │     └── templates  
   │         ├── nxos_bgp.j2  
   │         └── nxos_ip.j2  
   ├── ex2  
   │     ├── exercise2.yaml  
   │     └── group_vars  
   │         └── all.yaml  
   ├── ex3  
   │     ├── ASSEMBLE  
   │     │     ├── nxos1  
   │     │     │     ├── 10-nxos1-ip.cfg  
   │     │     │     └── 20-nxos1-bgp.cfg  
   │     │     └── nxos2  
   │     │         ├── 10-nxos2-ip.cfg  
   │     │         └── 20-nxos2-bgp.cfg  
   │     ├── cisco_nxos_show_ip_bgp_summary.template  
   │     ├── CONF  
   │     │     ├── nxos1.cfg  
   │     │     └── nxos2.cfg  
   │     ├── exercise1.yaml  
   │     ├── exercise3a.yaml  
   │     ├── exercise3b.yaml  
   │     ├── group_vars  
   │     │     ├── all.yaml  
   │     │     └── nxos.yaml  
   │     ├── host_vars  
   │     │     ├── nxos1.yaml  
   │     │     └── nxos2.yaml  
   │     └── templates  
   │         ├── nxos_bgp.j2  
   │         └── nxos_ip.j2  
   ├── ex4  
   │     ├── exercise4a.yaml  
   │     └── exercise4b.yaml  
   ├── ex5  
   │     ├── exercise5a.yaml  
   │     └── exercise5b.yaml  
   └── ex6  
       └── dyn_inv.py  
  
## lesson_8-Additional_Ansible_Techniques_and_Debugging  
├── Lesson_8.odt  
├── Lesson_8.txt  
└── work_files  
   ├── ex1  
   │     ├── cisco_nxos_show_lldp_neighbors.template  
   │     └── exercise1.yaml  
   ├── ex2  
   │     ├── ansible-hosts.ini  
   │     └── exercise2.yaml  
   ├── ex3  
   │     └── exercise3.yaml  
   ├── ex4  
   │     └── exercise4.yaml  
   ├── ex5  
   │     └── exercise5.yaml  
   ├── ex6  
   │     ├── cisco_nxos_show_lldp_neighbors.template  
   │     ├── exercise6a.yml  
   │     ├── exercise6b.yml  
   │     ├── exercise6c.yml  
   │     ├── exercise6d.yml  
   │     ├── exercise6e.yml  
   │     ├── exercise6f.yml  
   │     ├── exercise6g.yml  
   │     └── solutions  
   │         ├── cisco_nxos_show_lldp_neighbors.template  
   │         ├── exercise6a_fixed.yml  
   │         ├── exercise6b_fixed.yml  
   │         ├── exercise6c_fixed.yml  
   │         ├── exercise6d_fixed.yml  
   │         ├── exercise6e_fixed.yml  
   │         ├── exercise6f_fixed.yml  
   │         └── exercise6g_fixed.yml  
   └── ex7  
       ├── exercise7.yaml  
       └── group_vars  
           └── arista  
               ├── dns.yml  
               └── ntp.yml  
  
## lesson_9-NAPALM-Ansible  
├── Lesson_9.odt  
├── Lesson_9.txt  
└── work_files  
   ├── ex1  
   │     ├── BUP  
   │     │     ├── eos  
   │     │     │     ├── arista5.txt  
   │     │     │     ├── arista6.txt  
   │     │     │     ├── arista7.txt  
   │     │     │     └── arista8.txt  
   │     │     ├── ios  
   │     │     │     ├── cisco1.txt  
   │     │     │     ├── cisco2.txt  
   │     │     │     ├── cisco5.txt  
   │     │     │     └── cisco6.txt  
   │     │     ├── junos  
   │     │     │     ├── vmx1.txt  
   │     │     │     └── vmx2.txt  
   │     │     └── nxos  
   │     │         ├── nxos1.txt  
   │     │         └── nxos2.txt  
   │     └── exercise1.yaml  
   ├── ex2  
   │     └── exercise2.yaml  
   ├── ex3  
   │     ├── ASSEMBLE  
   │     │     └── eos  
   │     │         ├── 10-vlan.cfg  
   │     │         └── 20-interfaces.cfg  
   │     ├── CONFIGS  
   │     │     ├── eos  
   │     │     └── eos.cfg  
   │     ├── DIFF  
   │     │     └── arista5.txt  
   │     ├── exercise3.yaml  
   │     ├── group_vars  
   │     │     └── arista  
   │     │         ├── interfaces.yaml  
   │     │         └── vlans.yaml  
   │     └── templates  
   │         └── eos  
   │             ├── interfaces.j2  
   │             └── vlans.j2  
   ├── ex4  
   │     ├── ASSEMBLE  
   │     │     ├── nxos1  
   │     │     │     ├── 10-nxos1-ip.cfg  
   │     │     │     └── 20-nxos1-bgp.cfg  
   │     │     └── nxos2  
   │     │         ├── 10-nxos2-ip.cfg  
   │     │         └── 20-nxos2-bgp.cfg  
   │     ├── CONF  
   │     │     ├── nxos1.cfg  
   │     │     └── nxos2.cfg  
   │     ├── DIFF  
   │     │     ├── nxos1.txt  
   │     │     └── nxos2.txt  
   │     ├── exercise4.yaml  
   │     ├── group_vars  
   │     │     ├── all.yaml  
   │     │     └── nxos.yaml  
   │     ├── host_vars  
   │     │     ├── nxos1.yaml  
   │     │     └── nxos2.yaml  
   │     └── templates  
   │         ├── nxos_bgp.j2  
   │         └── nxos_ip.j2  
   └── ex5  
       ├── ASSEMBLE  
       │     └── eos  
       │         ├── 10-vlan.cfg  
       │         └── 20-interfaces.cfg  
       ├── CONFIGS  
       │     └── eos  
       │         ├── arista5-new_config.cfg  
       │         └── arista5-retrieved.cfg  
       ├── DIFF  
       │     └── arista5.txt  
       ├── exercise5-main.yaml  
       ├── exercise5-retrive.yaml  
       ├── group_vars  
       │     └── arista  
       │         ├── interfaces.yaml  
       │         └── vlans.yaml  
       └── templates  
           └── eos  
               ├── arista5.j2  
               ├── interfaces.j2  
               └── vlans.j2  
  
## playground  
Messing around spot.   
  
  
